# Runner Commands Integration Tests

This test suite is designed to actually exercise the gitlab-runner binary and test some of it's command line behaviours.

## Register test suite

The Register test suite, in register.py, attempts to exercise every available command line flag that the gitlab-runner exposes.

There are really two types of flags it looks at: those that get persisted to the config.toml file, and those that get persisted to the gitlab server.

For values that are persisted to a config file, the tests diff the created config.toml with an expected config file found in the `expected-outputs/` directory.

For values that are persisted to the gitlab server, the test first sets a requirement on the mock gitlab server that these flags MUST be present in the registration request. Then, if they are missing from the request the mock gitlab-server returns a `401` HTTP code and then registration fails.

A future enhancement would be to run something like:

```yaml
gitlab-runner register -h 2>&1 | tail -n +9 > flags_available.txt
diff flags_available.txt flags_tested.txt
```

Where `flags_tested.txt` is a list of the flags we have tested in the test suite so that we don't accidentally introduce new, untested, flags.

