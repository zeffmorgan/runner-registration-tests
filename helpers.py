import server

import subprocess
import threading
import urllib.request

# run_runner takes in the command and args for it
# args can either be a list of the different params or a string
# (as if you had copied it from the command line)
# returns a subprocess.CompletedProcess object
# https://docs.python.org/3/library/subprocess.html#subprocess.CompletedProcess
# which has as accessible variables, among other things,
# returncode, stdout, and stderr
def run_runner(cmd, args):
  if type(args) is type([]):
    return subprocess.run(["gitlab-runner", cmd] + args, capture_output=True)
  else:
    argList = args.split(' ')
    return subprocess.run(["gitlab-runner", cmd] + argList, capture_output=True)

# Launch the mock gitlab server in the background.
# Setting daemon = True means we don't need to kill ourselves
def run_gitlab():
  thread = threading.Thread(target=server.run, args=())
  thread.daemon = True
  thread.start()

# Set a required expectation (flag that MUST be sent) to the mock gitlab server
def set_gitlab_expectation(param, value):
  urllib.request.urlopen('http://localhost:8080/config/require/' + param + '/' + value)

# Reset all required expectations
def reset_gitlab_expectations():
  urllib.request.urlopen('http://localhost:8080/config/reset')

# Diff the config files
def diff_config(a, b):
  return subprocess.run(["diff", a, b], capture_output=True)

# Generate a filename to use as the expected config.toml filename from the test case name.
# The testname format we're expecting here is something like:
#    '__main__.TestRegisterCommands.test_basic_register'
# and we want to return "expected-outputs/basic-register.toml"
def filename_from_testname(testname):
  return "expected-outputs/" + testname.split('.')[-1:][0].replace('_','-').replace('test-','') + ".toml"
