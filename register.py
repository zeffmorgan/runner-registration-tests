import helpers

import os
import time
import unittest

TEST_CONFIG='test-config.toml'

class TestRegisterCommands(unittest.TestCase):

  @classmethod
  def setUpClass(cls):
    helpers.run_gitlab()

  def setUp(self):
    if os.path.exists(TEST_CONFIG):
     os.remove(TEST_CONFIG)
    self.expected_filename = helpers.filename_from_testname(unittest.TestCase.id(self))

  def tearDown(self):
    if os.path.exists(TEST_CONFIG):
     os.remove(TEST_CONFIG)
    helpers.reset_gitlab_expectations()
    time.sleep(1)

  def expect_matching_configs(self):
    d = helpers.diff_config(TEST_CONFIG, self.expected_filename)
    self.assertEqual(d.stderr, b"")
    self.assertEqual(d.stdout, b"")
    if d.returncode != 0:
      print("STDERR: ")
      print(d.stderr)
    self.assertEqual(d.returncode, 0)
  
  def expect_successful_runner(self, cmd, args):
    x = helpers.run_runner(cmd, args)
    if x.returncode != 0:
      print("STDERR: ")
      print(x.stderr)
    self.assertEqual(x.returncode, 0)

  def test_basic_invocation(self):
    self.expect_successful_runner('register',['--non-interactive','--url=http://localhost:8080','--config', TEST_CONFIG ,'--name','runna','--registration-token','1234567890','--executor','docker','--docker-image','alpine:latest','--docker-tlsverify=false','--docker-privileged=false','--docker-shm-size','0','--output-limit=40000','--docker-cpuset-cpus','1'])
    self.expect_matching_configs()

  def test_basic_invocation_with_short_flags(self):
    self.expect_successful_runner('register','-n -u=http://localhost:8080 -c ' + TEST_CONFIG + ' --name short-flags -r 1234567890 --executor shell')
    self.expect_matching_configs()

  def test_ssh_values(self):
    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name runna -r 1234567890 --executor ssh --ssh-password supersecret --ssh-user root --ssh-host localhost --ssh-port 42 --ssh-identity-file id_rsa.pub')
    self.expect_matching_configs()

  def test_tls_cert_values(self):
    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name tls --registration-token 1234567890 --executor shell --tls-ca-file ca.file --tls-cert-file cert.file --tls-key-file key.file')
    self.expect_matching_configs()

  def test_docker_advanced(self):
    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name docker-advanced --registration-token 1234567890 --executor docker --docker-image ubuntu:latest --docker-tlsverify --docker-privileged --docker-shm-size 1024 --output-limit=4321 --docker-cpuset-cpus 16 --docker-host localhost --docker-cert-path foo/bar --docker-hostname docker-hostname --docker-runtime test-runtime --docker-memory 2g --docker-memory-swap 512m --docker-memory-reservation 4096k --docker-cpus 8 --docker-cpu-shares 2 --docker-dns 8.8.8.8,8.8.4.4 --docker-dns-search gitlab.com --docker-disable-entrypoint-overwrite --docker-userns foonamespace --docker-cap-add NET_ADMIN --docker-cap-drop DAC_OVERRIDE --docker-oom-kill-disable --docker-oom-score-adjust 42 --docker-security-opt supersecure --docker-devices /dev/kvm --docker-disable-cache --docker-volumes /volumnes/foo --docker-volume-driver ext3 --docker-cache-dir ./foo/cache/dir/ --docker-extra-hosts registry.local:172.16.90.37 --docker-volumes-from /volumnes/inherited --docker-network-mode test-network --docker-links mysql_container:mysql --docker-wait-for-services-timeout 60 --docker-allowed-images ruby:*,python:*php:* --docker-allowed-services postgres:9,redis:*,mysql:* --docker-pull-policy if-not-present --docker-tmpfs /var/lib/mysql:rw,noexec --docker-services-tmpfs /var/lib/mysql:rw,noexec --docker-sysctls net.ipv4.ip_forward:1 --docker-helper-image helpme ')
    self.expect_matching_configs()

  def test_parallels_advanced(self):
    self.expect_successful_runner('register', '--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name parallels-advanced --registration-token 1234567890 --executor parallels --parallels-base-name windows-10-base --parallels-template-name windows-10-templ --parallels-disable-snapshots --parallels-time-server localhost.ntp --ssh-host localhost')
    self.expect_matching_configs()

  def test_virtualbox_advanced(self):
    self.expect_successful_runner('register', '--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name virtualbox-advanced --registration-token 1234567890 --executor virtualbox --virtualbox-base-name windows-10-base --virtualbox-base-snapshot windows-10-templ --virtualbox-disable-snapshots --ssh-host localhost --ssh-user test-user ')
    self.expect_matching_configs()

  def test_cache_s3(self):
    self.expect_successful_runner('register', '--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name cache --registration-token 1234567890 --executor shell --cache-type s3 --cache-path our-cache --cache-shared --cache-s3-server-address s3-host:123 --cache-s3-access-key s3AccessKey --cache-s3-secret-key s3SecretKey --cache-s3-bucket-name cache-bucket --cache-s3-bucket-location us-east-1 --cache-s3-insecure ')
    self.expect_matching_configs()

  def test_cache_gcs(self):
    self.expect_successful_runner('register', '--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name cache --registration-token 1234567890 --executor shell --cache-type gcs --cache-path our-cache --cache-gcs-access-id runner-id --cache-gcs-private-key private.key --cache-gcs-credentials-file credentials.file --cache-gcs-bucket-name gcs-cache-bucket ')
    self.expect_matching_configs()

  def test_kubernetes(self):
    self.expect_successful_runner('register', '--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name k8s --registration-token 1234567890 --executor kubernetes --kubernetes-host k8s.host --kubernetes-cert-file cert.file --kubernetes-key-file key.file --kubernetes-ca-file ca.file --kubernetes-bearer_token_overwrite_allowed --kubernetes-bearer_token t0ken --kubernetes-image alpine:latest --kubernetes-namespace k8s-ns --kubernetes-namespace_overwrite_allowed ci-${CI_COMMIT_REF_SLUG} --kubernetes-privileged --kubernetes-cpu-limit 1.0 --kubernetes-memory-limit 2g --kubernetes-service-cpu-limit 1 --kubernetes-service-memory-limit 512m --kubernetes-helper-cpu-limit 1 --kubernetes-helper-memory-limit 256m --kubernetes-cpu-request 1 --kubernetes-memory-request 256m --kubernetes-service-cpu-request 1 --kubernetes-service-memory-request 256m --kubernetes-helper-cpu-request 2 --kubernetes-helper-memory-request 1g --kubernetes-pull-policy always --kubernetes-node-selector foo.bar.com/node-pool:staging --kubernetes-node-tolerations key=value:effect --kubernetes-image-pull-secrets secret1,secret2 --kubernetes-helper-image helpMe:latest --kubernetes-terminationGracePeriodSeconds 42 --kubernetes-poll-interval 42 --kubernetes-poll-timeout 42 --kubernetes-pod-labels app:runner --kubernetes-service-account pod-acct --kubernetes-service_account_overwrite_allowed foo --kubernetes-pod-annotations anno:tation --kubernetes-pod_annotations_overwrite_allowed bar --kubernetes-pod-security-context-fs-group 42 --kubernetes-pod-security-context-run-as-group 99 --kubernetes-pod-security-context-run-as-non-root true --kubernetes-pod-security-context-run-as-user 1 --kubernetes-pod-security-context-supplemental-groups 5 ')
    self.expect_matching_configs()

  def test_docker_machine(self):
    self.expect_successful_runner('register',['--non-interactive','--url=http://localhost:8080','--config', TEST_CONFIG, '--name', 'machine','--registration-token', '1234567890', '--executor', 'docker+machine', '--docker-image', 'fuzz:latest', 
                                                '--machine-idle-nodes', '50', '--machine-idle-time', '60', '--machine-max-builds', '13', '--machine-machine-driver', 'amazon-ec2',
                                                '--machine-machine-name', 'runner-worker-%s', '--machine-machine-options', 'digitalocean-image=coreos-stable',
                                                '--machine-off-peak-periods', '* * 0-8,18-23 * * mon-fri *', '--machine-off-peak-timezone', 'UTC', 
                                                '--machine-off-peak-idle-count', '3', '--machine-off-peak-idle-time', '1800'])
    self.expect_matching_configs()

  def test_custom(self):
    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name custom --registration-token 1234567890 --executor custom --custom-config-exec /bin/config --custom-config-args foo,bar --custom-config-exec-timeout 300 --custom-prepare-exec /bin/prepare --custom-prepare-args baz,buz --custom-prepare-exec-timeout 600 --custom-run-exec /bin/run --custom-run-args aaa,bbb --custom-cleanup-exec /bin/tidyup --custom-cleanup-args cc,dd --custom-cleanup-exec-timeout 180 --custom-graceful-kill-timeout 30 --custom-force-kill-timeout 60')
    self.expect_matching_configs()

  def test_powershell(self):
    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name powershell --registration-token 1234567890 --executor shell --builds-dir /my/builds --cache-dir /my/cache --clone-url foo.git --env ONE=TWO --pre-clone-script ./pre-clone.sh --pre-build-script ./pre-build.sh --post-build-script ./post-build.sh --debug-trace-disabled --shell powershell --custom_build_dir-enabled')
    self.expect_matching_configs()

  def test_bools(self):
    helpers.set_gitlab_expectation("run_untagged", "true")
    helpers.set_gitlab_expectation("locked", "true")
    helpers.set_gitlab_expectation("active", "false") # for --paused q

    self.expect_successful_runner('register','--non-interactive --url=http://localhost:8080 --config ' + TEST_CONFIG + ' --name bools-true --registration-token 1234567890 --executor shell --custom_build_dir-enabled --paused --locked --leave-runner --run-untagged')
    self.expect_matching_configs()

  def test_run_untagged(self):
    helpers.set_gitlab_expectation("run_untagged", "true")

    # Flag set, and tags
    self.expect_successful_runner('register','register --non-interactive --url=http://localhost:8080 --config test-config.toml --name run-untagged-1 --registration-token 1234567890 --executor shell --run-untagged --tag-list foo,bar')

    # Flag set, no tags
    self.expect_successful_runner('register','register --non-interactive --url=http://localhost:8080 --config test-config.toml --name run-untagged-2 --registration-token 1234567890 --executor shell --run-untagged')

    # No flag, no tags
    self.expect_successful_runner('register','register --non-interactive --url=http://localhost:8080 --config test-config.toml --name run-untagged-3 --registration-token 1234567890 --executor shell')

    helpers.set_gitlab_expectation("run_untagged", "false")
    # No flag, but with tags
    self.expect_successful_runner('register','register --non-interactive --url=http://localhost:8080 --config test-config.toml --name run-untagged-4 --registration-token 1234567890 --executor shell --tag-list foo,bar')
    # untagged is a server side setting - we don't need to validate the config.toml output


if __name__ == '__main__':
  unittest.main()
